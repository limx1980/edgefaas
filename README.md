# FaaS Runtimes on Edge devices

One of our goals for the EdgeFaaS was to minimize the footprint of adding new functions to Edge devices. IoT devices tend to be limited in memory and storage, and some are located in remote locations, connected to the rest of the world via slow, expensive cellphone connections. This desire for lightweight function installation led to the modification of the OpenFaaS runtime to support multiple functions per container. With this modification, installing a new function is as small as the text file containing the function code, whereas original optimized Python function containers were at least 50MB in size.

## System specifications
The goal of our project is to move cloud services to the edge and IoT devices, so we chose the Raspberry Pi 4, a widely available small development board to develop the EdgeFaaS runtime. The Raspberry Pi 4 dev board we used features a Quad core Cortex-A72 SoC and 2GB of RAM. Our development board was running Raspbian 10 Buster with Linux 4.19. We used Docker 19.03.4 with [experimental] features enabled.

## Building the Docker image
Note: if you're planning to run EdgeFaaS using Kubernetes, you'd need to push your image to a registry.

### Building the image locally
The `build.sh` script was designed to build EdgeFaaS images locally and, if desired, push to the specified container registry. I designed it so it pushes the partial and builder containers into the registry. This is useful because these registry images (and their layers) can be used as caches, speeding up the build process.

If you would like to push your images to a registry, make sure be logged in.

```
$ docker login registry.example.com:<optional_port>
<truncated output>
```

The `build.sh` script accepts the following optional arguments:

* `-r`: OPTIONAL. Specify URL of where to push the built images. If not defined, the script will build locally. Usage: `./build.sh -r registry.example.com:4567/group/edgefaas`
* `-a`: OPTIONAL. Specify target architecture of the built images. If not defined, the script defaults to `arm` (`armhf`). Valid values are `arm`, `arm64` and `amd64`. Usage: `./build.sh -a amd64`

We can now run the script!

```
$ ./build.sh -r registry.example.com:4567/group/edgefaas -a arm
<truncated output>
$ docker images | grep latest
registry.example.com:4567/group/edgefaas   latest   dd05346b10c4          34 minutes ago   65.1MB
```
Note: if the images did not exist in the registry, you might see an error that the manifest for the image was not found. This error is safe to ignore and the script should continue running.

The `build.sh` script is designed to tag images with the git repo's commit hash. The final image tag is just this hash, but the tags of the partial and builder containers end in `part` and `wdog` respectively. The script also tags the most recently build images with the following tags: `<BRANCH>` for the full container, `<BRANCH>-part` for the partial container, and `<BRANCH>-wdog` for the builder container.

### Building and hosting the image remotely using GitLab
To further automate the build process for this project, I integrated it with GitLab's Continuous Integration (CI) service. Our GitLab is setup with GitLab Runners running in Docker containers on `amd64` machines. These jobs run in Docker-in-Docker with experimental features enabled. To learn more about our setup see [Eric's blogpost].

This code repository is also hosted on GitLab and it is setup in such that the CI jobs, specified in the repo's `.gitlab-ci.yml` file, run when a new commit is pushed into GitLab.

To build, simply push your updated code into GitLab, triggering a set of Jobs that will create images for `arm`, `arm64` and `amd64`.

## Launching the EdgeFaaS runtime
To reduce complexity, our EdgeFaaS system runs as a standalone Docker container. Running as a single container reduces scheduling and networking difficulties when deploying EdgeFaaS. Multiple EdgeFaaS containers can independently run in the same host machine.

### Running EdgeFaaS via Docker
The EdgeFaaS runtime is built into a Docker container, so users can directly use the Docker runtime to run EdgeFaaS. We setup EdgeFaaS so it is easy to set up by hand. Running EdgeFaaS via Docker is great for developing code and for small-scale deployments where you have access to the target machines.

```
$ docker run --name edgefaas -d --rm registry.gitlab.com/arm-research/smarter/edgefaas/edgefaas:v1-0-1
Unable to find image 'registry.gitlab.com/arm-research/smarter/edgefaas/edgefaas:v1-0-1' locally
latest: Pulling from arm-research/smarter/edgefaas
<truncated output>
Digest: sha256:5b78ca0b5762d46dff529ecc2ca7c0bae37071291818ee3b2c88ac46bd890e35
Status: Downloaded newer image for registry.gitlab.com/arm-research/smarter/edgefaas/edgefaas:v1-0-1
662e00dac7ec975e876d8f20bdbc961a3198fdc081e649dff2c107b87d059d85
```

Now, let's find the container's IP address. I'll store it as an environment variable to not have to retype it. Since we did not expose any ports, this container is only accessible from the host machine, but you can set it up differently.

```
$ docker inspect edgefaas | grep -i ipaddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
$ FAAS=172.17.0.2
```

Test the EdgeFaaS runtime using the default `echo` function. The default port is `8080`.

```
$ curl $FAAS:8080/echo -d "Welcome to EdgeFaaS!"
Welcome to EdgeFaaS!
```

To manage functions, feel free to skip to [Managing Functions](#managing-functions).

### Running EdgeFaaS via Kubernetes
To create an EdgeFaaS pod, we must provide an object specification to Kubernetes. The most common way to deploy Kubernetes objects is to use a `.yaml` file to send this information to the Kubernetes API via `kubectl`.

```
$ cat edgefaas.yaml 
apiVersion: v1
kind: Pod
metadata:
    name: edgefaas
spec:
    containers:
    - name: edgefaas
      image: registry.gitlab.com/arm-research/smarter/edgefaas/edgefaas:v1-0-1
      ports:
      - containerPort: 8080
  ```

Start EdgeFaaS using the `edgefaas.yaml`

```
$ kubectl create -f edgefaas.yaml 
pod/edgefaas created
```

If you want to run the EdgeFaaS pod directly, you can run the following command.

```
$ kubectl run edgefaas --image=registry.gitlab.com/arm-research/smarter/edgefaas/edgefaas:v1-0-1 --generator=run-pod/v1
pod/edgefaas created
```

Note: `--generator=run-pod/v1` tells Kubernetes to create a pod object using the image.

Retrieve the EdgeFaaS pod IP address and set an environment variable to it.

```
$ kubectl describe pod/edgefaas | grep IP
IP:           10.42.0.19
IPs:
  IP:  10.42.0.19
$ FAAS=10.42.0.19
```

Test the EdgeFaaS runtime using the default `echo` function. The default port is `8080`.

```
$ curl $FAAS:8080/echo -d "Welcome to EdgeFaas on k3s!"
Welcome to EdgeFaas on k3s!
```

## Managing Functions
One of our goals for the EdgeFaaS was to minimize the footprint of installing new functions. The biggest feature I added to the original `watchdog` code was enabling the runtime to add and remove functions on the fly, while the functions share a container. In this section, I describe how to manage functions for EdgeFaaS.

Each installed function can be triggered using the function name as a path, `edge.faas.ip:8080/function_name`. 

EdgeFaaS exposes an HTTP endpoint supporting the following HTTP methods: GET, POST, and DELETE. The management interface can be accessed via the `/mgmt` path.

### GET: listing installed functions
The GET method simply lists all the installed functions. Each line prints the function same, which is the same as the path use to trigger the function. In the future, GET could be expanded to print more information about the functions and the runtime.

```
$ curl edge.faas.ip:8080/mgmt
echo
func1
func2
...
funcn
```

Example:

```
$ curl $FAAS:8080/mgmt
echo
other_handler
```

### POST: installing functions
The POST method is used to add a function to the EdgeFaaS container. The POST method accepts an URL pointing to a file containing function following the specifications described in [Function Format](#function-format).  
The function file will be downloaded by the runtime, and the name of the function will be the filename with the extension truncated.

```
$ curl edge.faas.ip:8080/mgmt -d "https://my.site/func/new_func.py"
new_func.py has been created
```

Example:

```
$ curl $FAAS:8080/mgmt -d "https://gitlab.com/arm-research/smarter/edgefaas/edgefaas/raw/master/sample_functions/other_handler.py"
other_handler.py has been created
$ curl $FAAS:8080/other_handler
You're calling the other handler
$ curl $FAAS:8080/mgmt
echo
other_handler
```

Installed functions can be overwritten, and this is how function updates are executed. The only function that can't be overwritten is `echo` function since it is considered the default function. `echo` is used as a quick system check, and some EdgeFaaS functionality depends on it. 

### DELETE: deleting functions
The DELETE method is removes a function from the runtime. DELETE accepts a function name the argument. 

```
$ curl -X "DELETE" edge.faas.ip:8080/mgmt
```

DELETE in action:

```
$ curl $FAAS:8080/mgmt
echo
other_handler
$ curl -X "DELETE" $FAAS:8080/mgmt -d "other_handler"
$ curl $FAAS:8080/mgmt
echo
$ curl -X "DELETE" $FAAS:8080/mgmt -d "echo"
$ curl $FAAS:8080/mgmt
echo
```

If you try to delete the `echo` function, the system will not allow it since this is considered the default function.

### Function Format
The initial version of EdgeFaaS supports Python 2.7 functions. Future versions will support other programming languages.

Please keep the following in mind when writing functions for EdgeFaaS:

* The name of file containing the function must end in `.py`. The runtime will use the filename (minus `.py`) as the function name
* The file must contain a `handle` with the signature described below. The runtime will call `handle` when the function is triggered
* Any `pip` packages required by the any future function should be listed in the `requirements.txt` file on the `function` directory _before_ the EdgeFaaS image is created. 

We decided to keep the OpenFaaS Python function format.
The function file contains a function with the following signature:

```
def handle(req):
	<your_code>
	return req
```

If you're interested in seeing a valid file, please see the default function, located in the `function/handler.py` file, but please refrain from modifying this file.

## Limitations
EdgeFaaS started as an experiment within Arm Research to prototype cloud services deployed on the edge.

* The runtime currently supported is `Python 2.7`. New languages should not be difficult to add, especially those supported by OpenFaaS.
* For the time being, EdgeFaaS does not support installing new `pip` packages on the fly (without rebuilding/relaunching the runtime). Necessary packages should be listed in `function/requirements.txt` and they will be included in the runtime when building the container image.
* EdgeFaaS does not authenticate users. Anyone with network access to the container port can modify and trigger functions.
* No container runtime sharing between tenants. Each tenant should run their functions in their own runtime container because there is no strong separation between functions inside a container. 

[//]: # (REFERENCES)
[experimental]: https://github.com/docker/docker-ce/blob/master/components/cli/experimental/README.md#use-docker-experimental
[k3s]: https://github.com/rancher/k3s/blob/master/README.md
[kubeadm]: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
[Eric's blogpost]: https://community.arm.com/developer/research/b/articles/posts/continuous-cross-architecture-integration-with-gitlab
