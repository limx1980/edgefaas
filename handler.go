// Copyright (c) Alex Ellis 2017. All rights reserved.
// Copyright (c) Arm Ltd 2020
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/openfaas/faas/watchdog/types"
)

type requestInfo struct {
	headerWritten bool
}

// buildFunctionInput for a GET method this is an empty byte array
func buildFunctionInput(config *WatchdogConfig, r *http.Request) ([]byte, error) {
	var res []byte
	var requestBytes []byte
	var err error

	if r.Body == nil {
		return res, nil
	}
	defer r.Body.Close()

	if err != nil {
		log.Println(err)
		return res, err
	}

	requestBytes, err = ioutil.ReadAll(r.Body)
	if config.marshalRequest {
		marshalRes, marshalErr := types.MarshalRequest(requestBytes, &r.Header)
		err = marshalErr
		res = marshalRes
	} else {
		res = requestBytes
	}

	return res, err
}

// debugHeaders prints HTTP headers as key/value pairs
func debugHeaders(source *http.Header, direction string) {
	for k, vv := range *source {
		fmt.Printf("[%s] %s=%s\n", direction, k, vv)
	}
}

func pipeRequest(config *WatchdogConfig, w http.ResponseWriter, r *http.Request, method string) {
	startTime := time.Now()

	ri := &requestInfo{}

	if config.debugHeaders {
		debugHeaders(&r.Header, "in")
	}

	log.Println("Forking fprocess.")

	path := r.URL.Path
	log.Println("Path: " + path)

	fullPath := "function" + path + "/index.py"
	targetCmd := exec.Command("python", fullPath)

	envs := getAdditionalEnvs(config, r, method)
	if len(envs) > 0 {
		targetCmd.Env = envs
	}

	writer, _ := targetCmd.StdinPipe()

	var out []byte
	var err error
	var requestBody []byte

	var wg sync.WaitGroup

	wgCount := 2

	var buildInputErr error
	requestBody, buildInputErr = buildFunctionInput(config, r)
	if buildInputErr != nil {
		ri.headerWritten = true
		w.WriteHeader(http.StatusBadRequest)
		// I.e. "exit code 1"
		w.Write([]byte(buildInputErr.Error()))

		// Verbose message - i.e. stack trace
		w.Write([]byte("\n"))
		w.Write(out)

		return
	}

	wg.Add(wgCount)

	var timer *time.Timer

	if config.execTimeout > 0*time.Second {
		timer = time.AfterFunc(config.execTimeout, func() {
			log.Printf("Killing process: %s\n", config.faasProcess)
			if targetCmd != nil && targetCmd.Process != nil {
				ri.headerWritten = true
				w.WriteHeader(http.StatusRequestTimeout)

				w.Write([]byte("Killed process.\n"))

				val := targetCmd.Process.Kill()
				if val != nil {
					log.Printf("Killed process: %s - error %s\n", config.faasProcess, val.Error())
				}
			}
		})
	}

	// Write to pipe in separate go-routine to prevent blocking
	go func() {
		defer wg.Done()
		writer.Write(requestBody)
		writer.Close()
	}()

	if config.combineOutput {
		// Read the output from stdout/stderr and combine into one variable for output
		go func() {
			defer wg.Done()

			out, err = targetCmd.CombinedOutput()
		}()
	} else {
		go func() {
			var b bytes.Buffer
			targetCmd.Stderr = &b

			defer wg.Done()

			out, err = targetCmd.Output()
			if b.Len() > 0 {
				log.Printf("stderr: %s", b.Bytes())
			}
			b.Reset()
		}()
	}

	wg.Wait()
	if timer != nil {
		timer.Stop()
	}

	if err != nil {
		if config.writeDebug == true {
			log.Printf("Success=%t, Error=%s\n", targetCmd.ProcessState.Success(), err.Error())
			log.Printf("Out=%s\n", out)
		}

		if ri.headerWritten == false {
			w.WriteHeader(http.StatusInternalServerError)
			response := bytes.NewBufferString(err.Error())
			w.Write(response.Bytes())
			w.Write([]byte("\n"))
			if len(out) > 0 {
				w.Write(out)
			}
			ri.headerWritten = true
		}
		return
	}

	var bytesWritten string
	if config.writeDebug == true {
		os.Stdout.Write(out)
	} else {
		bytesWritten = fmt.Sprintf("Wrote %d Bytes", len(out))
	}

	if len(config.contentType) > 0 {
		w.Header().Set("Content-Type", config.contentType)
	} else {

		// Match content-type of caller if no override specified
		clientContentType := r.Header.Get("Content-Type")
		if len(clientContentType) > 0 {
			w.Header().Set("Content-Type", clientContentType)
		}
	}

	execDuration := time.Since(startTime).Seconds()
	if ri.headerWritten == false {
		w.Header().Set("X-Duration-Seconds", fmt.Sprintf("%f", execDuration))
		ri.headerWritten = true
		w.WriteHeader(200)
		w.Write(out)
	}

	if config.debugHeaders {
		header := w.Header()
		debugHeaders(&header, "out")
	}

	if len(bytesWritten) > 0 {
		log.Printf("%s - Duration: %f seconds", bytesWritten, execDuration)
	} else {
		log.Printf("Duration: %f seconds", execDuration)
	}
}

func getAdditionalEnvs(config *WatchdogConfig, r *http.Request, method string) []string {
	var envs []string

	if config.cgiHeaders {
		envs = os.Environ()

		for k, v := range r.Header {
			kv := fmt.Sprintf("Http_%s=%s", strings.Replace(k, "-", "_", -1), v[0])
			envs = append(envs, kv)
		}

		envs = append(envs, fmt.Sprintf("Http_Method=%s", method))
		// Deprecation notice: Http_ContentLength will be deprecated
		envs = append(envs, fmt.Sprintf("Http_ContentLength=%d", r.ContentLength))
		envs = append(envs, fmt.Sprintf("Http_Content_Length=%d", r.ContentLength))

		if config.writeDebug {
			log.Println("Query ", r.URL.RawQuery)
		}

		if len(r.URL.RawQuery) > 0 {
			envs = append(envs, fmt.Sprintf("Http_Query=%s", r.URL.RawQuery))
		}

		if config.writeDebug {
			log.Println("Path ", r.URL.Path)
		}

		if len(r.URL.Path) > 0 {
			envs = append(envs, fmt.Sprintf("Http_Path=%s", r.URL.Path))
		}

		if len(r.Host) > 0 {
			envs = append(envs, fmt.Sprintf("Http_Host=%s", r.Host))
		}

	}

	return envs
}

func lockFilePresent() bool {
	path := filepath.Join(os.TempDir(), ".lock")
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

func createLockFile() (string, error) {
	path := filepath.Join(os.TempDir(), ".lock")
	log.Printf("Writing lock-file to: %s\n", path)
	writeErr := ioutil.WriteFile(path, []byte{}, 0660)

	atomic.StoreInt32(&acceptingConnections, 1)

	return path, writeErr
}

// Returns all the directories inside function directory.
// The directories should directly represent the functions
func getFilenames() (string, error) {
	var files []string
	f, err := os.Open("/home/app/function")

	if err != nil {
		return "ERROR", err
	}

	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return "ERROR", err
	}

	for _, file := range fileInfo {
		files = append(files, file.Name())
	}

	result := strings.Join(files, "\n") + "\n"

	return result, err
}

// This function downloads a file and returns filename
func downloadFile(urlname string) (string, error) {
	var err error
	log.Println("Downloading " + urlname)
	reply, err := http.Get(urlname)

	if err != nil {
		return urlname, err
	}

	splitURL := strings.Split(urlname, "/")
	filename := splitURL[len(splitURL)-1]

	// Remove file extension
	dirnameSplit := strings.Split(filename, ".")
	dirpath := "function/" + dirnameSplit[0]

	// Check if the name is echo (reserved name)
	// If echo, throw error
	if dirnameSplit[0] == "echo" {
		log.Println("cannot be named echo")
		return urlname, errors.New("WrongName")
	}

	// Copy template directory, the echo function
	log.Println("Copying template into " + dirpath)
	cmd := exec.Command("cp", "-r", "function/echo", dirpath)
	err = cmd.Run()
	if err != nil {
		log.Println("Error copying")
		return urlname, err
	}
	log.Println("Directory copied")

	// Where the file lives
	fullFilename := dirpath + "/handler.py"

	file, err := os.Create(fullFilename)

	defer file.Close()

	if err != nil {
		return urlname, err
	}

	_, err = io.Copy(file, reply.Body)

	log.Println("File downloaded")
	return filename, err
}

// This function downloads the specifcied file.
// This function then creates a directory using the file name
// with all the necessary files
func downloadAndSetup(w http.ResponseWriter, r *http.Request) {
	var err error
	if r.Body == nil {
		log.Println("ERROR: bad request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	requestBytes, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	payload := string(requestBytes)
	log.Println("Pulling from " + payload)

	filename, err := downloadFile(payload)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(filename + " has been created\n"))

	log.Println("File downloaded and directory created")
	return
}

func deleteFunction(w http.ResponseWriter, r *http.Request) {
	var err error
	if r.Body == nil {
		log.Println("ERROR: bad request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	requestBytes, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	payload := string(requestBytes)
	log.Println("Deleting function " + payload)

	filepath := "/home/app/function/" + payload

	// Check if file exits
	if _, err := os.Stat(filepath); err != nil {
		log.Println("Could not find and delete " + payload)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Check if path equals echo
	if payload == "echo" {
		log.Println("Cannot delete echo, the default function")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	log.Println("Removing " + payload)
	err = os.RemoveAll(filepath)

	if err != nil {
		log.Println("ERROR: Error removing function")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	log.Println("Function " + payload + " removed")
	return
}

// Use this function to handle function status/creation/etc
func makeManagementHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("Mgmt request received")
		switch r.Method {
		case http.MethodGet:
			log.Println("Get detected")
			name, err := getFilenames()
			if err != nil {
				log.Println("ERROR: getFilename")
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(name))
			break
		case http.MethodPost:
			log.Println("Post detected")
			downloadAndSetup(w, r)
			break
		case http.MethodDelete:
			log.Println("Delete detected")
			deleteFunction(w, r)
			break
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func makeHealthHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			if atomic.LoadInt32(&acceptingConnections) == 0 || lockFilePresent() == false {
				w.WriteHeader(http.StatusServiceUnavailable)
				return
			}

			w.WriteHeader(http.StatusOK)
			w.Write([]byte("OK"))

			break
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func makeRequestHandler(config *WatchdogConfig) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodGet:
			pipeRequest(config, w, r, r.Method)
			break
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)

		}
	}
}
