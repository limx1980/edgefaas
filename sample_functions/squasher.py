# Copyright (c) Arm Ltd 2020
# Licensed under the MIT license. See LICENSE file in the project root for full license information.
import json

def main():
    sound='{"status": "ok", "predictions": [{"label_id": "/m/06mb1", "label": "Rain", "probability": 0.7376469373703003}, {"label_id": "/m/0ngt1", "label": "Thunder", "probability": 0.60517817735672}, {"label_id": "/t/dd00038", "label": "Rain on surface", "probability": 0.5905200839042664}, {"label_id": "/m/0jb2l", "label": "Thunderstorm", "probability": 0.5793699026107788}, {"label_id": "/m/07yv9", "label": "Vehicle", "probability": 0.34878015518188477}]}'

    print "INPUT:"
    print sound
    out = handle("")
    
    print "\n\nOUTPUT:"
    print out

    return

def handle(req):

    try:
        parsed = json.loads(req)

        dictionary = {}
        idx = 0

        for item in parsed['predictions']:
            lbl = "label" + str(idx)
            prb = "probability" + str(idx)
            dictionary[lbl] = item['label']
            dictionary[prb] = item['probability']
            idx += 1

        del parsed['predictions']
        parsed.update(dictionary)

        return json.dumps(parsed)

    except Exception as e:
        return ('{\"Exception\": "' + str(e) + '"}')

if __name__== "__main__":
  main()

