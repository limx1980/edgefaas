#!/bin/bash

set -e

#
# THIS SCRIPT ONLY SUPPORTS ONE ARCHITECTURE AT A TIME. RIGHT NOW ARMHF
#

usage () {
  printf "Usage: build.sh \n [-r] image url within container registry, leave empty to keep images locally \n [-a] target architecture e.g arm, arm64, amd64 (default arm)"
}

while getopts "hr:a:" opt; do
  case ${opt} in
    h )
	    usage && exit
        ;;
    r )
        REGISTRY=$OPTARG
        ;;
    a ) 
        TARGETARCH=$OPTARG
        ;;
    \? ) echo "Invalid flag passed. Use -h flag for script options" && exit
      ;;
  esac
done

# Set default args
if [ -z $TARGETARCH ]
then
    TARGETARCH=arm
fi

echo "Target arch set to $TARGETARCH"

GIT_COMMIT=$(git rev-list -1 HEAD)
VERSION=$(git describe --all --exact-match `git rev-parse HEAD` | grep tags | sed 's/tags\///')
BRANCH=$(git rev-parse --abbrev-ref HEAD)

CI_COMMIT_SHA=$GIT_COMMIT

# Remove python:2.7-alpine image to account for differnences in arch
docker rmi python:2.7-alpine &> /dev/null|| true

if [[ -n $REGISTRY ]]
then
    echo "Make sure you've logged into the registry"
    echo "Runnng registry build..."
    CONTAINER_IMAGE=$REGISTRY
else
    echo "Running local build..."
    CONTAINER_IMAGE=edgefaas
fi

CONTAINER_FULL_IMAGE=$CONTAINER_IMAGE:${BRANCH}
CONTAINER_PART_IMAGE=$CONTAINER_IMAGE:${BRANCH}-part
CONTAINER_WDOG_IMAGE=$CONTAINER_IMAGE:${BRANCH}-wdog

#####

if [[ -n $REGISTRY ]]
then
    # Try to pull image if it exists
    echo "Attempting to pull $CONTAINER_PART_IMAGE"
    docker pull $CONTAINER_PART_IMAGE || true
fi

echo "Building $CONTAINER_PART_IMAGE"
docker build -f Dockerfile.func --cache-from $CONTAINER_PART_IMAGE -t $CONTAINER_PART_IMAGE -t $CONTAINER_IMAGE:${CI_COMMIT_SHA}-part --platform linux/$TARGETARCH .

if [[ -n $REGISTRY ]]
then
    echo "Pushing $CONTAINER_PART_IMAGE"
    docker push $CONTAINER_PART_IMAGE
    docker push $CONTAINER_IMAGE:${CI_COMMIT_SHA}-part
fi

#####

if [[ -n $REGISTRY ]]
then
    echo "Attempting to pull $CONTAINER_WDOG_IMAGE"
    docker pull $CONTAINER_WDOG_IMAGE || true
fi

echo "Building $CONTAINER_WDOG_IMAGE"
docker build -f Dockerfile.wdog --cache-from $CONTAINER_WDOG_IMAGE -t $CONTAINER_WDOG_IMAGE -t $CONTAINER_IMAGE:${CI_COMMIT_SHA}-wdog --build-arg VERSION=$VERSION --build-arg GIT_COMMIT=$GIT_COMMIT .

if [[ -n $REGISTRY ]]
then
    echo "Pushing $CONTAINER_WDOG_IMAGE"
    docker push $CONTAINER_WDOG_IMAGE
    docker push $CONTAINER_IMAGE:${CI_COMMIT_SHA}-wdog
fi

#####

# Extract goods from builder container
docker create --name builder $CONTAINER_WDOG_IMAGE echo
docker cp builder:/go/src/github.com/openfaas/faas/watchdog/watchdog-$TARGETARCH ./fwatchdog-$TARGETARCH
docker rm builder

#####

if [[ -n $REGISTRY ]]
then
    echo "Attempting to pull $CONTAINER_FULL_IMAGE"
    docker pull $CONTAINER_FULL_IMAGE || true
fi

echo "Building $CONTAINER_FULL_IMAGE"
docker build -f Dockerfile.full --cache-from $CONTAINER_FULL_IMAGE -t $CONTAINER_FULL_IMAGE -t $CONTAINER_IMAGE:${CI_COMMIT_SHA} --platform linux/$TARGETARCH --build-arg TARGETARCH=$TARGETARCH --build-arg IMAGE_PART=$CONTAINER_PART_IMAGE .

if [[ -n $REGISTRY ]]
then
    echo "Pushing $CONTAINER_FULL_IMAGE"
    docker push $CONTAINER_FULL_IMAGE
    docker push $CONTAINER_IMAGE:$CI_COMMIT_SHA
fi
